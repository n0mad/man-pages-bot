#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "rtfm_local.h"

//4096 seems max message size for a message. Consider split in several msgs
#define BUFLEN 4096

char *buffer;
long file_size;

int calculate_num_msgs() {
	//if file size is greater than 4096, we need to split the message
	//into several chunks of 4096 max
	int num_msgs = 0;
	while(file_size > 0) {
		num_msgs++;
		file_size -= BUFLEN;
	}

	printf("num msgs is: %d\nfile size is %ld\n", num_msgs, file_size);

	return num_msgs;
}

void split_message(char **messages, int num_msgs) {
	//split page into 4096 chunks
	for(int i = 0; i < num_msgs; i++) {
		messages[i] = (char*)malloc(BUFLEN * sizeof(char));
	}
	
	int sum = 0;
	for(int i = 0; i < num_msgs; i++) {
		(char*)strlcpy(messages[i], &buffer[sum], sizeof(char) * BUFLEN);
		sum += BUFLEN;
	}
	
}

void read_file(char *file_path) {
	FILE *fp = fopen(file_path, "r");

	if(!fp) {
		printf("error opening file");
		exit(1);
	}

	fseek(fp, 0, SEEK_END);
	file_size = ftell(fp);
	rewind(fp);

	printf("file size is %ld\n", file_size);

	buffer = (char*)malloc(file_size * sizeof(char));
	
	fread(buffer, file_size, 1, fp);
	
	if(file_size >= BUFLEN) {
		printf("message size is greater than MAX\n");
	}

	fclose(fp);
}

