#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <ctype.h>
#include "telebot.h"
#include "rtfm_local.h"
#include "rtfm_curl.h"
#include "rtfm_commands.h"

#define SIZE_OF_ARRAY(array) (sizeof(array) / sizeof(array[0]))
#define MAX_ARGS 3

const char *local_path = "./";

app_state_t *app_state;

/*int main(int argc, char **argv) {

	app_state.send_man_file = false;
	
	char *s_cmd[MAX_ARGS];
	char *msg = "/man 1 wait";
	parse_command(msg, s_cmd, MAX_ARGS);
	
	for(int i = 0; i < MAX_ARGS; i++) {
		free(s_cmd[i]);
	}

	read_file("./temp_raw.txt");
	int num_msgs = calculate_num_msgs();

	//char **messages = NULL;
	char *messages[num_msgs];

	split_message(messages, num_msgs);
	for(int i = 0; i < 2; i++) {
		printf("message[%d] is:\n", i);
		printf("%s\n", messages[i]);
		free(messages[i]);
	}

	return 0;
}*/


int main(int argc, char **argv) {

	app_state = calloc(1, sizeof(app_state_t));

	app_state->send_man_file = false;
	app_state->current_args = 0;
	app_state->max_args = MAX_ARGS;
	app_state->local_man_file = malloc(128 * sizeof(char));
	app_state->temp_file_name = malloc(128 * sizeof(char));

	char *s_args[app_state->max_args];

	for(int i = 0; i < app_state->max_args; i++) {
		s_args[i] = (char*)malloc(128 * sizeof(char));
		//s_args[i] = "";
	}
    
	printf("Welcome to rtfm-bot\n");

    FILE *fp = fopen(".token", "r");
    if (fp == NULL) {
        printf("Failed to open .token file\n");
        return -1;
    }

    char token[1024];
    if (fscanf(fp, "%s", token) == 0) {
        printf("Failed to read token\n");
        fclose(fp);
        return -1;
    }
    // printf("Token: %s\n", token);
    fclose(fp);

    telebot_handler_t handle;
    if (telebot_create(&handle, token) != TELEBOT_ERROR_NONE) {
        printf("Telebot create failed\n");
        return -1;
    }

    telebot_user_t me;
    if (telebot_get_me(handle, &me) != TELEBOT_ERROR_NONE) {
        printf("Failed to get bot information\n");
        telebot_destroy(handle);
        return -1;
    }

	printf("ID: %d\n", me.id);
	printf("First Name: %s\n", me.first_name);
	printf("User Name: %s\n", me.username);

	telebot_put_me(&me);

	int index, count, offset = -1;
	telebot_error_e ret;
	telebot_message_t message;
	telebot_update_type_e update_types[] = {TELEBOT_UPDATE_TYPE_MESSAGE};


	while (1) {
		telebot_update_t *updates;
		ret = telebot_get_updates(handle, offset, 20, 0, update_types, 0, &updates, &count);
		if (ret != TELEBOT_ERROR_NONE)
			continue;
		printf("Number of updates: %d\n", count);
        for (index = 0; index < count; index++) {
            message = updates[index].message;
            if (message.text) {
                printf("%s: %s \n", message.from->first_name, message.text);

				parse_command(message.text, s_args, app_state);

				char str[4096];
				int num_msgs = 0;
				
				if (strstr(s_args[0], "/start")) {
					snprintf(str, SIZE_OF_ARRAY(str),\
							"Hello %s!", message.from->first_name);
					ret = telebot_send_message(handle, message.chat->id, str, "HTML",\
							true, false, updates[index].message.message_id, "");

					if (ret != TELEBOT_ERROR_NONE) {
						printf("Failed to send message: %d \n", ret);
					}
				}
				if (strstr(s_args[0], "/sendfile")) {
					app_state->send_man_file = send_man_file(s_args[1]);
					printf("send as file has been set to %d\n", app_state->send_man_file);
				}
				if (strstr(s_args[0], "/man")) {

					parse_args(s_args, app_state);

					strcpy(app_state->local_man_file, local_path);
					strcat(app_state->local_man_file, app_state->temp_file_name);
					printf("filename set to %s\n", app_state->local_man_file);

					if(app_state->send_man_file != true) {
						printf("send as file is %d\n", app_state->send_man_file);
						// get man msgs to send
						read_file(app_state->local_man_file);
						num_msgs = calculate_num_msgs();
						char *messages[num_msgs];
						split_message(messages, num_msgs);

						for(int i = 0; i < num_msgs; i++) {
							ret = telebot_send_message(handle, message.chat->id, messages[i],\
									"HTML", true, false, updates[index].message.message_id,\
									"");

							if (ret != TELEBOT_ERROR_NONE) {
								printf("Failed to send message: %d \n", ret);
							}
							free(messages[i]);
						}
					} else {
						printf("send as file is enabled\n");

						ret = telebot_send_document(handle, message.chat->id, app_state->local_man_file, true, NULL, "", "HTML", false, true, "");
						
						if (ret != TELEBOT_ERROR_NONE) {
							printf("Failed to send message: %d \n", ret);
						}
					}

					app_state->current_args = 0;
					if(remove(app_state->local_man_file) == 0)
						printf("removed temp %s\n", app_state->local_man_file);
				}
				
            }
            offset = updates[index].update_id + 1;
        }
        telebot_put_updates(updates, count);

        sleep(1);
    }

	for(int i = 0; i < MAX_ARGS; i++) {
		free(s_args[i]);
	}
	free(app_state);
    telebot_destroy(handle);

    return 0;
}

