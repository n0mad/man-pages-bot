#include <curl/curl.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "rtfm_curl.h"

//as an alternative to support more *nix variants we could switch to
//char *test_url = "https://nixdoc.net/man-pages/FreeBSD/man1/alloc.1.html";

const char *base_url = "https://www.freebsd.org/cgi/man.cgi?query=";
const char *url_mid = "&sektion=";
const char *url_tail = "&manpath=FreeBSD+13.1-RELEASE+and+Ports&format=ascii";

size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream) {
  size_t doc = fwrite(ptr, size, nmemb, (FILE *)stream);
  return doc;
}

int online_man(char *cmd, char *page, app_state_t *app_state) {

	CURL *curl = curl_easy_init();

	int file_name_len = strlen(cmd) + 9;
	char *temp_page = malloc(file_name_len * sizeof(char));
	
	strcpy(temp_page, cmd);
	strcat(temp_page, "_man.txt");

	app_state->temp_file_name = temp_page;
	
	FILE* file = NULL;

	if(!curl) {
		printf("failed to init curl\n");
		return EXIT_FAILURE;
	}

	char *url = NULL;	
	int url_len = strlen(base_url) + strlen(url_mid) + strlen(url_tail)\
				  + strlen(cmd) + strlen(page) + 1;

	url = malloc(url_len * sizeof(char));

	strcpy(url, base_url);
	strcat(url, cmd);
	strcat(url, url_mid);
	if(strlen(page) != 0) {
		strcat(url, page);
		strcat(url, url_tail);
	} else {
		strcat(url, "1");
		strcat(url, url_tail);
	}

	printf("%s\n", url);

	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);

	
	file = fopen(temp_page, "w");
	
	if(file) {
 
    	curl_easy_setopt(curl, CURLOPT_WRITEDATA, file);
		CURLcode res = curl_easy_perform(curl);

		if(res != CURLE_OK) {
			printf("failed to download url: %s\n", curl_easy_strerror(res));
			return EXIT_FAILURE;	
		}
 
		fclose(file);
	}	
	curl_easy_cleanup(curl);
	free(url);
	return EXIT_SUCCESS;
}

