#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "rtfm_commands.h"
#include "rtfm_curl.h"

void parse_command(char *cmd, char **args, app_state_t *app_state) {
	//split string in pieces
	//first should be /man
	//second one can be either man page (int) or argument directly (string)
	//if the second one is a number, we need to reach the third one to get the command to man
	
	app_state->current_args = 0;
	char *dup_cmd, *token, *rest;

	//this is required to have the string in heap. Otherwise we get segmentation fault
	dup_cmd = strdup(cmd);
	rest = dup_cmd;

	while((token = strtok_r(rest, " \t", &rest)) && app_state->current_args < app_state->max_args) {
		args[app_state->current_args] = token;
		printf("split is: %s\n", args[app_state->current_args]);
		app_state->current_args++;
	}
	
	//printf("current args is %d\n", app_state->current_args);

	free(dup_cmd);
	free(token);
	free(rest);

	/*for(int i = 0; i < app_state.max_args; i++) {
		free(args[i]);
	}*/

}

void parse_args(char **args, app_state_t *app_state) {
	if (app_state->current_args < 1) {
		printf("current args is %d\n", app_state->current_args);
	} else if(app_state->current_args == app_state->max_args) {
		int check = atoi(args[1]);
		// printf("check for args[1] %s is %d\n", args[1], check);
		
		if(check != 0) {
			printf("check not 0 is %d\n", check);
			online_man(args[2], args[1], app_state);
		}

	} else if(app_state->current_args == 2) {
		printf("split msg is cmd: %s, man %s\n", args[0], args[1]);
		online_man(args[1], "", app_state);
	}

}

