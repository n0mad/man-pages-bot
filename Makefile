CC = cc

SRCS != ls src/*.c
OBJS = $(SRCS:.c=.o)

INC = -Iinclude -I/usr/local/include -I/usr/local/include/curl -I/usr/local/include/json-c
CFLAGS = -c -g -Wall $(INC)
LDFLAGS = -L/usr/local/lib -L./libs/ -lcurl -ltelebot -ljson-c

EXEC_NAME = test

## build exec ##
all: output

output: $(OBJS)
	@echo Building $(EXEC_NAME) ...
	$(CC) $(OBJS) $(LDFLAGS) -o $(EXEC_NAME)

$(OBJS): $(SRCS)
	@echo Compiling srcs ...
	$(CC) $(CFLAGS) -c $< -o $@

## remove things ##
.PHONY: clean

clean:
	rm src/*.o $(EXEC_NAME)
	clear
