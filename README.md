# RTFM | FreeBSD man pages bot

Telegram bot to serve man pages on demand for FreeBSD. It also offers links to the handbook.

## Commands

Here's the actual list of available commands for the bot:

### search man page

```sh
/man arg1 arg2
```

If the bot only receives `arg1` then it tries to search that `arg` in the man's db. In this case `arg1` has to be a string.

If the bot receives both `arg1` and `arg2` then it's supposed to read `arg1` as the man page index, and `arg2` as the command to look for in the man's db.


### serve man page settings

```sh
/sendfile arg1
```

The `arg1` has to receive either `true` or `false`. If set to `true`, then the man page is sent as an attachment. If set to `false` then the bot replies in plain text serving the man page.


### list handbook

```sh
/docindex
```

This is a WIP feature, meant to serve FreeBSD's handbook pages

### handbook page

```sh
/docs arg1
```

WIP: The `arg1` has to be a valid name from the list provided by `/docindex`

