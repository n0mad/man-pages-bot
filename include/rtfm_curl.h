#ifndef RTFM_CURL_H
#define RTFM_CURL_H

#include "rtfm_common.h"

int online_man(char *cmd, char *page, app_state_t *app_state);

#endif // RTFM_CURL_H
