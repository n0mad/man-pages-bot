#ifndef RTFM_LOCAL_H
#define RTFM_LOCAL_H

//man seems to use a lot of U+0008 backspaces when writing names. This 
//makes the text to be displayed wrongly. To deprecate this method
//char *local_man(char *cmd);

void read_file(char *file_path);

/*
 * if file size is greater than 4096, we need to split the message
 * into several chunks of 4096 max
 */
int calculate_num_msgs();

/*
 * split page into 4096 chunks
 */
void split_message(char **messages, int num_msgs);

#endif // RTFM_LOCAL_H

