#ifndef RTFM_COMMON_H
#define RTFM_COMMON_H

#ifndef bool
typedef enum {false, true} bool;
#endif

typedef struct {
	bool send_man_file;
	int current_args, max_args;
	char *local_man_file;
	char *temp_file_name;
} app_state_t;

bool send_man_file(char *set);

#endif // RTFM_COMMON_H

