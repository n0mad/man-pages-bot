#ifndef RTFM_COMMANDS_H
#define RTFM_COMMANDS_H

#include "rtfm_common.h"

void parse_command(char *cmd, char **args, app_state_t *app_state);

void parse_args(char **args, app_state_t *app_state);

#endif //RTFM_COMMANDS_H

